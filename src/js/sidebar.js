(function(){

      // Toggle button
  var toggle = document.getElementById( "toggle" ),
      // Navbar element wrap
      collapse = document.querySelector(".sidebar-wrapper");

  // Add a click event
  toggle.addEventListener("click", function() {

    // get the aria-expanded value
    var colValue = collapse.getAttribute("aria-expanded");

     switch(colValue) {
       case "true":
         // turn expanded=false
         collapse.setAttribute("aria-expanded","false");

         break;
       case "false":
         // turn expanded=true
         collapse.setAttribute("aria-expanded","true");
         break;
     }

  }, false);
})();


//detect scroll event and add a class
$(window).on('scroll', function() {

  var html = document.querySelector("html");
  var body = document.querySelector("body");

  var sidebar = document.querySelector(".sidebar-wrapper");

  var bodyScrollVal = body.scrollTop;
  var htmlScrollVal = html.scrollTop;

  if( bodyScrollVal > 1 || htmlScrollVal > 1 ) {

    sidebar.classList.add("is-docked");

  } else {

    if( bodyScrollVal === 0 || htmlScrollVal === 0) {

      sidebar.classList.remove("is-docked");

    }
  }
});

  var didScroll = false;

  $(window).scroll(function () {
      didScroll = true;
  });

  setInterval(function () {
      if (didScroll) {
          didScroll = false;
          // Check your page position and then
          // Load in more results
          var html = document.querySelector("html");
          var body = document.querySelector("body");

          var sidebar = document.querySelector(".sidebar-wrapper");

          var bodyScrollVal = body.scrollTop;
          var htmlScrollVal = html.scrollTop;

          if( bodyScrollVal > 1 || htmlScrollVal > 1 ) {

            if($(".sidebar-wrapper").hasClass("is-docked")) {
              return false;
            } else {
              sidebar.classList.add("is-docked");
            }

          } else if( bodyScrollVal === 0 || htmlScrollVal === 0) {

              sidebar.classList.remove("is-docked");
          }
      }
  }, 250);

